/*****************************************************************************
 *
 * Real-Time Example with a Cyclic Thread, EtherCAT Data and PdServ Support
 *
 * Based on
 * https://wiki.linuxfoundation.org/realtime/documentation \
 * /howto/applications/application_base
 *
 * and
 * https://wiki.linuxfoundation.org/realtime/documentation \
 * /howto/applications/cyclic
 *
 ****************************************************************************/

#define _GNU_SOURCE

#include <limits.h>
#include <pthread.h>
#include <sched.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>

#include <ecrt.h> // EtherCAT Realtime Interface
#include <pdserv.h> // EtherCAT Realtime Interface

#define PERIOD_NS 1000000 // task period in [ns]

#define NSEC_PER_SEC 1000000000 // nanoseconds per second

#define DIFF_NS(A, B) \
    (((B).tv_sec - (A).tv_sec) * NSEC_PER_SEC + (B).tv_nsec - (A).tv_nsec)

#define MY_STACK_SIZE 8192

/****************************************************************************/

static struct pdserv *pdserv = NULL;
static struct pdtask *pdtask = NULL;
static double anaInVoltage = 0.0;

static ec_master_t *master = NULL;
static ec_domain_t *domain = NULL;
static ec_domain_state_t domain_state = {};

static uint8_t *domain_pd = NULL;

//                     Alias, Position
#define AnaInSlavePos      0, 4

//                       Vendor ID, Product Code
#define Beckhoff_EL3104 0x00000002, 0x0c203052

// offsets for PDO entries
static unsigned int off_ana_in;

const static ec_pdo_entry_reg_t domain_regs[] = {
//  {Alias, Position, Vendor ID, Product Code, PDO Index, -Subindex, Offset}
    {AnaInSlavePos,   Beckhoff_EL3104,         0x6000, 0x11, &off_ana_in},
    {}
};

/****************************************************************************/

/* Master 0, Slave 4, "EL3104"
 * Vendor ID:       0x00000002
 * Product code:    0x0c203052
 * Revision number: 0x00110000
 */

ec_pdo_entry_info_t el3104_pdo_entries[] = {
    {0x6000, 0x11, 16},
    {0x6010, 0x11, 16},
    {0x6020, 0x11, 16},
    {0x6030, 0x11, 16},
};

ec_pdo_info_t el3104_pdos[] = {
    {0x1a01, 1, el3104_pdo_entries + 0},
    {0x1a03, 1, el3104_pdo_entries + 1},
    {0x1a05, 1, el3104_pdo_entries + 2},
    {0x1a07, 1, el3104_pdo_entries + 3},
};

ec_sync_info_t el3104_syncs[] = {
    {0, EC_DIR_OUTPUT, 0, NULL, EC_WD_DISABLE},
    {1, EC_DIR_INPUT, 0, NULL, EC_WD_DISABLE},
    {2, EC_DIR_OUTPUT, 0, NULL, EC_WD_DISABLE},
    {3, EC_DIR_INPUT, 4, el3104_pdos + 0, EC_WD_DISABLE},
    {0xff}
};

/*****************************************************************************/

void check_domain_state(void)
{
    ec_domain_state_t ds;

    ecrt_domain_state(domain, &ds);

    if (ds.working_counter != domain_state.working_counter) {
        printf("Domain: Working Counter changed to %u.\n",
                ds.working_counter);
    }
    if (ds.wc_state != domain_state.wc_state) {
        printf("Domain: Working Counter State changed to %u.\n", ds.wc_state);
    }

    domain_state = ds;
}

/****************************************************************************/

struct period_info {
    long period_ns;
    struct timespec next_period;
    struct timespec prev_start_time;
};

/****************************************************************************/

static void inc_period(struct period_info *pinfo)
{
    pinfo->next_period.tv_nsec += pinfo->period_ns;

    while (pinfo->next_period.tv_nsec >= NSEC_PER_SEC) {
        /* timespec nsec overflow */
        pinfo->next_period.tv_sec++;
        pinfo->next_period.tv_nsec -= NSEC_PER_SEC;
    }
}

/****************************************************************************/

static void periodic_task_init(struct period_info *pinfo)
{
    pinfo->period_ns = PERIOD_NS;

    /* find timestamp to first run */
    clock_gettime(CLOCK_MONOTONIC, &pinfo->next_period);
    pinfo->prev_start_time = pinfo->next_period;
}

/****************************************************************************/

static void do_rt_task(struct period_info *pinfo)
{
    struct timespec world_time, start_time, end_time;
    uint32_t exec_ns, period_ns;

    /* Take start times */
    clock_gettime(CLOCK_MONOTONIC, &start_time);
    clock_gettime(CLOCK_REALTIME, &world_time);

    /* Calculate task statistics */
    period_ns = DIFF_NS(pinfo->prev_start_time, start_time);
    pinfo->prev_start_time = start_time;

    /* Receive process data */
    ecrt_master_receive(master);
    ecrt_domain_process(domain);

    /* Check process data state */
    check_domain_state();

    /* Access process data */
    int16_t raw_value = EC_READ_S16(domain_pd + off_ana_in);
    anaInVoltage = raw_value * 10.0 / 32768.0;

    /* Send process data */
    ecrt_domain_queue(domain);
    ecrt_master_send(master);

    /* Update PdServ */
    pdserv_update(pdtask, &world_time);

    /* Update task statistics */
    clock_gettime(CLOCK_MONOTONIC, &end_time);
    exec_ns = DIFF_NS(start_time, end_time);
    pdserv_update_statistics(pdtask, exec_ns / 1e9, period_ns / 1e9, 0);
}

/****************************************************************************/

static void wait_rest_of_period(struct period_info *pinfo)
{
    inc_period(pinfo);

    /* for simplicity, ignoring possibilities of signal wakes */
    clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME,
            &pinfo->next_period, NULL);
}

/****************************************************************************/

void *simple_cyclic_task(void *data)
{
    struct period_info pinfo;

    periodic_task_init(&pinfo);

    while (1) {
        do_rt_task(&pinfo);
        wait_rest_of_period(&pinfo);
    }

    return NULL;
}

/****************************************************************************/

int init_ethercat()
{
    ec_slave_config_t *sc;

    master = ecrt_request_master(0);
    if (!master) {
        return -1;
    }

    domain = ecrt_master_create_domain(master);
    if (!domain) {
        return -1;
    }

    if (!(sc = ecrt_master_slave_config(master,
                    AnaInSlavePos, Beckhoff_EL3104))) {
        fprintf(stderr, "Failed to get slave configuration.\n");
        return -1;
    }

    printf("Configuring PDOs...\n");
    if (ecrt_slave_config_pdos(sc, EC_END, el3104_syncs)) {
        fprintf(stderr, "Failed to configure PDOs.\n");
        return -1;
    }

    // Register PDOs for exchange
    if (ecrt_domain_reg_pdo_entry_list(domain, domain_regs)) {
        fprintf(stderr, "PDO entry registration failed!\n");
        return -1;
    }

    printf("Activating master...\n");
    if (ecrt_master_activate(master)) {
        return -1;
    }

    if (!(domain_pd = ecrt_domain_data(domain))) {
        return -1;
    }

    return 0;
}

/****************************************************************************/

int init_pdserv()
{
    /* Create a PdServ instance */
    if (!(pdserv = pdserv_create("EtherCAT/PdServ", "0.99", NULL))) {
        printf("Failed to init PdServ.\n");
        return -1;
    }

    /* Create a task with a cycle time of 1 ms. */
    if (!(pdtask = pdserv_create_task(pdserv, PERIOD_NS * 1e-9, NULL))) {
        printf("Failed to create task.\n");
        return -1; /* failed to create a task. */
    }

    /* Register a signal */
    pdserv_signal(pdtask, 1, "/Io/AnaInVoltage",
            pd_double_T, &anaInVoltage, 1, 0);

    int ret = pdserv_prepare(pdserv); /* Forks a communication process */
    if (ret) {
        printf("Failed to prepare PdServ.\n");
        return -1; /* failed to create a task. */
    }

    return 0;
}

/****************************************************************************/

int main(int argc, char* argv[])
{
    struct sched_param param;
    pthread_attr_t attr;
    pthread_t thread;
    int ret;

    ret = init_pdserv();
    if (ret) {
        printf("Failed to init PdServ.\n");
        exit(-2);
    }

    ret = init_ethercat();
    if (ret) {
        printf("Failed to init EtherCAT master.\n");
        exit(-2);
    }

    /* Lock memory */
    if (mlockall(MCL_CURRENT | MCL_FUTURE) == -1) {
        printf("mlockall() failed: %m\n");
        exit(-2);
    }

    /* Initialize pthread attributes (default values) */
    ret = pthread_attr_init(&attr);
    if (ret) {
        printf("init pthread attributes failed\n");
        goto out;
    }

    /* Set a specific stack size  */
    ret = pthread_attr_setstacksize(&attr, PTHREAD_STACK_MIN + MY_STACK_SIZE);
    if (ret) {
        printf("pthread setstacksize failed\n");
        goto out;
    }

    /* Set scheduler policy and priority of pthread */
    ret = pthread_attr_setschedpolicy(&attr, SCHED_FIFO);
    if (ret) {
        printf("pthread setschedpolicy failed\n");
        goto out;
    }

    param.sched_priority = 80;
    ret = pthread_attr_setschedparam(&attr, &param);
    if (ret) {
        printf("pthread setschedparam failed\n");
        goto out;
    }

    /* Use scheduling parameters of attr */
    ret = pthread_attr_setinheritsched(&attr, PTHREAD_EXPLICIT_SCHED);
    if (ret) {
        printf("pthread setinheritsched failed\n");
        goto out;
    }

    /* Create a pthread with specified attributes */
    ret = pthread_create(&thread, &attr, simple_cyclic_task, NULL);
    if (ret) {
        printf("create pthread failed: %s\n", strerror(ret));
        goto out;
    }

    ret = pthread_setname_np(thread, "ethercat-rt");
    if (ret) {
        printf("failed to set thread name\n");
    }

    /* Join the thread and wait until it is done */
    ret = pthread_join(thread, NULL);
    if (ret) {
        printf("join pthread failed: %m\n");
    }

out:
    return ret;
}

/****************************************************************************/
