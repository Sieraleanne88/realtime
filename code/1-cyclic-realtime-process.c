/*****************************************************************************
 *
 * POSIX Real Time Example
 * using a single pthread as RT thread
 *
 * Based on
 * https://wiki.linuxfoundation.org/realtime/documentation \
 * /howto/applications/application_base
 *
 * and
 * https://wiki.linuxfoundation.org/realtime/documentation \
 * /howto/applications/cyclic
 *
 ****************************************************************************/

#define _GNU_SOURCE

#include <limits.h>
#include <pthread.h>
#include <sched.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>

#define MY_STACK_SIZE 8192

/****************************************************************************/

struct period_info {
    struct timespec next_period;
    long period_ns;
};

/****************************************************************************/

static void inc_period(struct period_info *pinfo)
{
    pinfo->next_period.tv_nsec += pinfo->period_ns;

    while (pinfo->next_period.tv_nsec >= 1000000000) {
        /* timespec nsec overflow */
        pinfo->next_period.tv_sec++;
        pinfo->next_period.tv_nsec -= 1000000000;
    }
}

/****************************************************************************/

static void periodic_task_init(struct period_info *pinfo)
{
    /* for simplicity, hardcoding a 1 ms period */
    pinfo->period_ns = 1000000;

    clock_gettime(CLOCK_MONOTONIC, &(pinfo->next_period));
}

/****************************************************************************/

static void do_rt_task()
{
    /* Do RT stuff here. */
}

/****************************************************************************/

static void wait_rest_of_period(struct period_info *pinfo)
{
    inc_period(pinfo);

    /* for simplicity, ignoring possibilities of signal wakes */
    clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME,
            &pinfo->next_period, NULL);
}

/****************************************************************************/

void *simple_cyclic_task(void *data)
{
    struct period_info pinfo;

    periodic_task_init(&pinfo);

    while (1) {
        do_rt_task();
        wait_rest_of_period(&pinfo);
    }

    return NULL;
}

/****************************************************************************/

int main(int argc, char* argv[])
{
    struct sched_param param;
    pthread_attr_t attr;
    pthread_t thread;
    int ret;

    /* Lock memory */
    if (mlockall(MCL_CURRENT | MCL_FUTURE) == -1) {
        printf("mlockall() failed: %m\n");
        exit(-2);
    }

    /* Initialize pthread attributes (default values) */
    ret = pthread_attr_init(&attr);
    if (ret) {
        printf("init pthread attributes failed\n");
        goto out;
    }

    /* Set a specific stack size  */
    ret = pthread_attr_setstacksize(&attr, PTHREAD_STACK_MIN + MY_STACK_SIZE);
    if (ret) {
        printf("pthread setstacksize failed\n");
        goto out;
    }

    /* Set scheduler policy and priority of pthread */
    ret = pthread_attr_setschedpolicy(&attr, SCHED_FIFO);
    if (ret) {
        printf("pthread setschedpolicy failed\n");
        goto out;
    }

    param.sched_priority = 80;
    ret = pthread_attr_setschedparam(&attr, &param);
    if (ret) {
        printf("pthread setschedparam failed\n");
        goto out;
    }

    /* Use scheduling parameters of attr */
    ret = pthread_attr_setinheritsched(&attr, PTHREAD_EXPLICIT_SCHED);
    if (ret) {
        printf("pthread setinheritsched failed\n");
        goto out;
    }

    /* Create a pthread with specified attributes */
    ret = pthread_create(&thread, &attr, simple_cyclic_task, NULL);
    if (ret) {
        printf("create pthread failed: %s\n", strerror(ret));
        goto out;
    }

    ret = pthread_setname_np(thread, "cyclic-rt-1-ms");
    if (ret) {
        printf("failed to set thread name\n");
    }

    /* Join the thread and wait until it is done */
    ret = pthread_join(thread, NULL);
    if (ret) {
        printf("join pthread failed: %m\n");
    }

out:
    return ret;
}

/****************************************************************************/
